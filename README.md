# Trabajo Final 

Este repositorio de __GITLAB__ se creo con el objetivo de compartir de forma academica el trabajo final.


__Materia  == Desarrollo Para El PenTesting__    
__Profesor == Cesar Anaya__  
__Alumno   == Juan Felipe Trujillo Toro__     
__Especialización en Ciberseguridad__  
__Universidad Católica de Manizalez__ 
    
__Link del Repositorio == https://gitlab.com/juan.trujillo/trabajofinalucm__  


__Definición__

<p style="text-align: justify;">
Este laboratorio y ejercicio, emula por medio del programa realizado en Python la suplantación del router de la maquina víctima, por medio de técnicas de spoofing, logrando que el trafico de la maquina victima sea enrutado hacia el KaliLinux que corre el programa, esto como técnica de ataque MitM, el mismo programa está en la capacidad de detectar la incorporación de los login y passwords introducidos para la autenticación en los sitios WEB y recopila la información en un formato json.
HTTP. </p>  

__MitM__
<p style="text-align: justify;"> 
Un ataque Man-in-the-Middle (MITM) es un tipo de ciberataque en el que los criminales interceptan una conversación o una transferencia de datos existente, ya sea escuchando o haciéndose pasar por un participante. A la víctima le parecerá que se está produciendo un intercambio de información normal, pero al introducirse en la conversación o transferencia de datos, el hacker puede obtener la información mientras pasa desapercibido.
    
El objetivo de un ataque MITM es obtener datos confidenciales, como detalles de cuentas bancarias, números de tarjetas de crédito o credenciales de inicio de sesión, que pueden utilizar para llevar a cabo otros delitos como el robo o la suplantación de identidad o las transferencias ilegales de fondos. Como los ataques MITM se llevan a cabo en tiempo real, suelen pasar desapercibidos hasta que es demasiado tarde.
</p>

__Escenario y Requisitos__

Conexion a Internet   
Portatil Windows   
Hypervisor Vmware WorkStation    
Maquina Virtual Kali Linux __(ATACANTE)__  
Python v.3.10.4   
  - JupyterLab 
  - Libreria Scapy v.2.4.5   
  - Libreria Scapy_HTTP  v.1.8.2  
Maquina Virtual Windows 7 __(VICTIMA)__  



Para la ejecucion de este laboratorio academico es necesario que el __vm_ATACANTE__ y la __vm_VICTIMA__ esten en el mismo segmento de RED (192.168.164.0/24) como se evidencia en la siguiente imangen:

![1](Diagrama_Ataque.png)

En la grafica anterior observamos que el ataque inicia con la ejecucion de un script unificado, codificado en python que comprometen la victima, obteniendo información confidencial de usuarios y contraseñas

__snifferRed.py__      ----> Modificas las tablas ARP de la Victima y el Gateway, ademas Captura Tráfico HTTP y Guarda Credenciales de Usuarios y Claves 
